import { Component, OnInit } from "@angular/core";
import { SOCIAL_MEDIA_ICONS } from "./socialMediaIcons";

@Component({
  selector: "app-footer",
  templateUrl: "./footer.component.html",
  styleUrls: ["./footer.component.css"],
})
export class FooterComponent implements OnInit {
  icons = SOCIAL_MEDIA_ICONS;
  constructor() {}

  ngOnInit(): void {}
}
