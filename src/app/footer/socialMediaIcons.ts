export const SOCIAL_MEDIA_ICONS = [
  {
    name: "facebook",
    iconName: "facebook",
    href: "https://www.facebook.com/SOS.Global/",
  },
  {
    name: "instagram",
    iconName: "instagram",
    href: "https://www.instagram.com/science.of.spirituality/",
  },
  {
    name: "youtube",
    iconName: "youtube-play",
    href: "https://www.youtube.com/user/SantRajinderSingh",
  },
  {
    name: "twitter",
    iconName: "twitter",
    href: "https://twitter.com/sosmeditate",
  },
];
