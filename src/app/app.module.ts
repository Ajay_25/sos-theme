import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { GalleryComponent } from "./gallery/gallery.component";

//testing-purpose-only
import { TopBarComponent } from "./test-components-and-services/top-bar/top-bar.component";
import { ProductListComponent } from "./test-components-and-services/product-list/product-list.component";
import { ProductAlertsComponent } from "./test-components-and-services/product-alerts/product-alerts.component";
import { ProductDetailsComponent } from "./test-components-and-services/product-details/product-details.component";
import { CartComponent } from "./test-components-and-services/cart/cart.component";
import { ShippingComponent } from "./test-components-and-services/shipping/shipping.component";

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([{ path: "", component: GalleryComponent }]),
  ],
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    GalleryComponent,

    /*Testing purpose only*/
    TopBarComponent,
    ProductListComponent,
    ProductAlertsComponent,
    ProductDetailsComponent,
    CartComponent,
    ShippingComponent,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

/*
Copyright Google LLC. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at https://angular.io/license
*/
